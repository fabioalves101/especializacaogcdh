-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 19/08/2013 às 17:57
-- Versão do servidor: 5.6.12-log
-- Versão do PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `especializacaogcdh`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `inscricao`
--

CREATE TABLE IF NOT EXISTS `inscricao` (
  `idinscricao` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(245) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `siape` varchar(11) DEFAULT NULL,
  `email` varchar(245) NOT NULL,
  `graduacao` varchar(245) NOT NULL,
  `graduacao_qual` varchar(255) DEFAULT NULL,
  `posgraduacao` varchar(245) DEFAULT NULL,
  `posgraduacao_qual` varchar(255) DEFAULT NULL,
  `observacoes` text,
  `poloid` int(11) NOT NULL,
  `data_admissao` varchar(25) DEFAULT NULL,
  `lotacao` varchar(255) DEFAULT NULL,
  `protocolo` varchar(25) DEFAULT NULL,
  `datacadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`idinscricao`),
  KEY `polofk_idx` (`poloid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `polo`
--

CREATE TABLE IF NOT EXISTS `polo` (
  `idpolo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `vagas` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpolo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Fazendo dump de dados para tabela `polo`
--

INSERT INTO `polo` (`idpolo`, `nome`, `vagas`) VALUES
(1, 'Araguaia', 30),
(2, 'Cuiaba', 60),
(3, 'Rondonopolis', 30),
(4, 'Sinop', 30);

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `inscricao`
--
ALTER TABLE `inscricao`
  ADD CONSTRAINT `polofk` FOREIGN KEY (`poloid`) REFERENCES `polo` (`idpolo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
