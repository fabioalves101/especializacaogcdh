<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Formulário de Inscrição 2013</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="assets/css/custommain.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="assets/img/favicon.ico">

    
 
<!-- biblioteca jquery -->
<script type="text/javascript" language="javascript" src="js/jquery-1.8.3.min.js" ></script>

<!-- bootstrap min -->
<script src="js/bootstrap.min.js"></script>

<!-- js mask for phone n cpf  -->
<script type="text/javascript" language="javascript" src="js/jquery.maskedinput-1.3.min.js"></script>

<!-- bootbox code | alerts with bootstrap -->
<script src="js/bootbox.min.js"></script>




<?php if ($_GET['showmsg'] == '1') {
echo "

<script type='text/javascript'>
$(window).load(function(){
	
$('#myModal').modal({
  backdrop: 'static',
  keyboard: false
})

});
</script>

";
} else { 
echo "

<!-- nothing to show -->

";
}
?>


</head>
<body>
<!-- load modal on page load -->
        <!-- Button to trigger modal -->
       
      <!--  <a href="#myModal" data-keyboard="false" data-backdrop="static" role="button" class="btn" data-toggle="modal">Launch demo modal</a> -->
        
        
        <!-- servidor -->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
    
        <h3 id="myModalLabel">Acesso Servidor</h3>
        </div>   
        <div class="modal-body">

   
<form action="index_servidor.php" class="bs-docs-example form-horizontal" method="post">

<div class="control-group">
<label class="control-label" for="inputEmail">SIAPE</label>
<div class="controls">
<input type="text" name="siape" />
</div>
</div>




<div class="control-group">
<div class="controls">

<button type="submit" name="prosseguir" class="btn">Prosseguir</button>

</div>
</div>

</form>
   
   
   
        </div>  
        <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Fechar</button>

        
        </div>
        </div>
        
        
        
        
        
        <!-- comunidade externa -->
        
         <div id="myModalExt" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
    
        <h3 id="myModalLabel">Acesso Comunidade Externa</h3>
        </div>   
        <div class="modal-body">
       

   
   
   
        </div>  
        <div class="modal-footer">
        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true">Li e Aceito as Instruções</button>

        
        </div>
        </div>
        
        
        
        
        
        
        
        
        
<!-- load modal on page load -->

















<div id="header-align">
	<header id="header">
 		 <div id="logo">
       	 <img src="assets/img/ufmtlogo.jpg" alt="ufmt">
         </div>
         
         <div id="departinfo">
       	   <span>Gerência de Capacitação e Desenvolvimento Humano</span>
            	<p>GCDH</p>
         </div>
		
   </header>
</div>

<div id="section-align">
	<div id="section-bgcompose">
	<section id="principalinfo">
		<div id="section-info">
        
            <h2>COMUNICADO</h2>
            <span class="title">Atenção mensagem....</span>    
            <p>Estão abertas as inscrições on line para participar do Projeto de Extensão</p>
            
            
            <span class="duv">Dúvidas pelo email abaixo</span>
            <span class="email">
            <a href="mailto:projetoeduquimica@hotmail.com">projetoeduquimica@gmail.com</a>
            </span>
            
       </div>

		
        
   </section>
   
   
   	<div id="section-choose">
	
<a href="index.php?showmsg=1" style="width:250px;height:90px;padding-top:90px; padding-bottom:20px; padding-left:43px; padding-right:30px; font-size:22px; margin-top:10px;" class="btn btn-large">Servidor
<p style="font-size:12px !important;">Acesso através do SIAPE</p>        
</a>

          
          
<a href="index_externa.php" style="width:250px;height:90px;padding-top:90px; padding-bottom:20px; padding-left:43px; padding-right:30px; font-size:22px; margin-top:10px;" class="btn btn-large">Comunidade Externa
<!--<p style="font-size:12px !important;">Acesso através do CPF</p>-->       
</a>


          
        
    </div>
    </div>
</div>






<div id="footer-align">
	<footer id="mainfooter">
		<p>GCDH- Gerência de Capacitação e Desenvolvimento Humano - UFMT<br>
Av. Fernando Corrêa da Costa, nº 2367 - Bairro Boa
Esperança. Cuiabá - MT - 78060-900</p>
    
    </footer>
</div>
 

<script data-main="assets/js/main-built.js" src="assets/js/lib/require.js" ></script>
</body>
</html>
