<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Formul&aacute;rio de Inscri&ccedil;&atilde;o - CURSO: Educa&ccedil;&atilde;o das Rela&ccedil;&otilde;es &Eacute;tnicorraciais no Contexto da Educa&ccedil;&atilde;o de Jovens e Adultos</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />


<!-- JS MASK E TELEFONE DATA -->
<script type="text/javascript" language="javascript" src="js-scripts/jquery-1.8.3.min.js" ></script>
<script type="text/javascript" language="javascript" src="js-scripts/jquery.maskedinput-1.3.min.js"></script>
<!-- JS MASK E TELEFONE DATA -->




<script language="JavaScript" >

function enviardados(){
<!-- ############### SELECTS ################ ->

<!--- validando radio box --->

<!--- sexo --->
if(document.getElementById("sexo1").checked == false && document.getElementById("sexo2").checked == false)
{
alert( "Escolha seu sexo." );
document.getElementById("sexo1").focus();
return false;
}
<!--- sexo --->


<!--- docente --->
if(document.getElementById("docente1").checked == false && document.getElementById("docente2").checked == false)
{
alert( "Docente?" );
document.getElementById("docente1").focus();
return false;
}
<!--- docente --->


<!--- leciona --->
if(document.getElementById("leciona1").checked == false && document.getElementById("leciona2").checked == false)
{
alert( "Selecione se você leciona." );
document.getElementById("leciona1").focus();
return false;
}
<!--- leciona --->





<!--- SELECIONE POLO --->
if(document.formulario.selecione_polo.value=="Selecione")
{
alert( "Selecione o polo de sua preferência." );
document.formulario.selecione_polo.focus();
return false;
}
<!--- SELECIONE POLO --->



<!--- SUA FORMAÇÃO --->

if(document.formulario.formacao.value=="Selecione sua Formação")
{
alert( "Selecione sua formação." );
document.formulario.formacao.focus();
return false;
}


if(document.formulario.formacao.value=="Graduando" && document.formulario.data_inicio.value=="" || document.formulario.formacao.value =="Especialização - Cursando" && document.formulario.data_inicio.value=="" || document.formulario.formacao.value =="Mestrando" && document.formulario.data_inicio.value=="" || document.formulario.formacao.value =="Doutorando" && document.formulario.data_inicio.value=="" || document.formulario.formacao.value =="Pós-Doutorando" && document.formulario.data_inicio.value=="")
{
alert( "Digite a data de início da formação." );
document.formulario.data_inicio.focus();
return false;
}


if(document.formulario.formacao.value=="Superior Completo" && document.formulario.data_defesa.value=="" || document.formulario.formacao.value =="Especialização - Completa" && document.formulario.data_defesa.value=="" || document.formulario.formacao.value =="Mestrado" && document.formulario.data_defesa.value=="" || document.formulario.formacao.value =="Doutorado" && document.formulario.data_defesa.value=="" || document.formulario.formacao.value =="Pós-Doutorado" && document.formulario.data_defesa.value=="")
{
alert( "Digite a data de defesa da formação." );
document.formulario.data_defesa.focus();
return false;
}	

<!--- SUA FORMAÇÃO --->


<!--- leciona JOVENS E ADULTOS --->
if(document.getElementById("lecionajovens1").checked == false && document.getElementById("lecionajovens2").checked == false)
{
alert( "Você leciona ou já lecionou para jovens e adultos?" );
document.getElementById("lecionajovens1").focus();
return false;
}
<!--- leciona JOVENS E ADULTOS --->



<!--- LECIONA --->


if(document.getElementById("leciona1").checked ==true && document.formulario.leciona_tempo.value=="")
{
alert( "Digite há quanto tempo leciona." );
document.formulario.leciona_tempo.focus();
return false;
}

if(document.getElementById("leciona1").checked ==true && document.formulario.leciona_em.value=="")
{
alert( "Selecione a série que leciona." );
document.formulario.leciona_tempo.focus();
return false;
}




if(document.getElementById("leciona2").checked ==true && document.formulario.leciona_funcao.value=="")
{
alert( "Selecione sua função." );
document.formulario.leciona_funcao.focus();
return false;
}


<!--- LECIONA --->



<!--- CURSO TEMATICA --->
if(document.getElementById("curso_tematica1").checked == false && document.getElementById("curso_tematica2").checked == false)
{
alert( "Escolha uma opção em curso a temática." );
document.getElementById("curso_tematica1").focus();
return false;
}


<!--- SE SIM -->
if(document.getElementById("curso_tematica1").checked ==true && document.formulario.curso_relacoes_instituicao.value=="")
{
alert( "Selecione qual instituição promoveu seu curso." );
document.formulario.curso_relacoes_instituicao.focus();
return false;
}
<!-- SE SIM -->

<!-- SE NAO -->
if(document.getElementById("leciona2").checked ==true && document.formulario.leciona_funcao.value=="Selecione")
{
alert( "Selecione sua função." );
document.formulario.leciona_funcao.focus();
return false;
}
<!-- SE NAO -->

<!--- CURSO TEMÁTICA --->


<!--- VINCULO --->
if(document.formulario.vinculo.value=="Selecione")
{
alert( "Selecione seu vínculo." );
document.formulario.vinculo.focus();
return false;
}
<!--- VINCULO --->


<!--- RAÇA --->
if(document.formulario.raca_ibge.value=="Selecione")
{
alert( "Selecione sua raça." );
document.formulario.raca_ibge.focus();
return false;
}
<!--- RAÇA --->


<!--- PDE -->
if(document.getElementById("pde1").checked == false && document.getElementById("pde2").checked == false)
{
alert( "Já se inscreveu no PDE?" );
document.getElementById("pde1").focus();
return false;
}
<!--- PDE -->



<!--- DISPONIBILIDADE COMP -->
if(document.getElementById("dispocomp1").checked == false && document.getElementById("dispocomp2").checked == false)
{
alert( "Disponibilidade de computador com internet?" );
document.getElementById("dispocomp1").focus();
return false;
}
<!--- DISPONIBILIDADE COMP -->




<!--- CURSO A DISTANCIA --->
if(document.getElementById("cursodis1").checked == false && document.getElementById("cursodis2").checked == false)
{
alert( "Você já fez algum curso em educação à Distancia?" );
document.getElementById("cursodis1").focus();
return false;
}


<!--- SE SIM -->
if(document.getElementById("cursodis1").checked ==true && document.formulario.curso_ed_dist_horas.value=="")
{
alert( "Qual o tempo de duração do curso." );
document.formulario.curso_ed_dist_horas.focus();
return false;
}
<!-- SE SIM -->



<!--- CURSO A DISTANCIA --->



<!--- NIVEL CONHECIMENTO INFORMATICA --->
if(document.formulario.nivel_conhecimento.value=="Selecione")
{
alert( "Selecione seu nível de conhecimento de informática." );
document.formulario.nivel_conhecimento.focus();
return false;
}
<!--- NIVEL CONHECIMENTO INFORMATICA --->


<!--- ACESSAR EMAIL --->
if(document.formulario.acessar_email.value=="Selecione")
{
alert( "Você costuma acessar seu email." );
document.formulario.acessar_email.focus();
return false;
}
<!--- ACESSAR EMAIL --->


<!--- HORAS BOM DESENVOLVIMENTO --->
if(document.formulario.horas_necessario.value=="")
{
alert( "Quantas horas você acredita ser necessário para se obter um bom desenvolvimento no ensino a distância?." );
document.formulario.horas_necessario.focus();
return false;
}
<!--- HORAS BOM DESENVOLVIMENTO --->



<!--- IMPEDIMENTO PARTICIPAR --->
if(document.getElementById("impedi1").checked == false && document.getElementById("impedi2").checked == false)
{
alert( "Possui algum impedimento para participar dos encontros presenciais aos sábados?" );
document.getElementById("impedi1").focus();
return false;
}



<!--- IMPEDIMENTO PARTICIPAR --->










<!--- NOME --->
if(document.formulario.nome.value=="" || document.formulario.nome.length < "2")
{
alert( "Digite seu nome completo." );
document.formulario.nome.focus();
return false;
}
<!--- NOME --->


<!--- CPF --->
if(document.formulario.cpf.value=="" || document.formulario.cpf.length < "2")
{
alert( "Digite seu CPF." );
document.formulario.cpf.focus();
return false;
}
<!--- CPF --->




<!--- endereço --->
if(document.formulario.endereco.value=="")
{
alert( "Digite sua endereço." );
document.formulario.endereco.focus();
return false;
}
<!--- endereço --->


<!--- bairro --->
if(document.formulario.bairro.value=="")
{
alert( "Digite seu bairro." );
document.formulario.bairro.focus();
return false;
}
<!--- bairro --->


<!--- cep --->
if(document.formulario.cep.value=="")
{
alert( "Digite seu Cep." );
document.formulario.cep.focus();
return false;
}
<!--- cep --->


<!--- cidade --->
if(document.formulario.cidade.value=="")
{
alert( "Digite sua Cidade." );
document.formulario.cidade.focus();
return false;
}
<!--- cidade --->

<!--- telefone --->
if(document.formulario.telefone.value=="")
{
alert( "Digite seu telefone." );
document.formulario.telefone.focus();
return false;
}
<!--- telefone --->





<!--- idade --->
if(document.formulario.idade.value=="")
{
alert( "Digite sua idade." );
document.formulario.idade.focus();
return false;
}
<!--- idade  --->






<!--- email --->

if(document.formulario.email.value=="")
{
alert( "Digite seu E-mail." );
document.formulario.email.focus();
return false;
}

<!--- email --->



<!--- uf --->
if(document.formulario.uf.value=="")
{
alert( "Selecione seu Estado." );
document.formulario.uf.focus();
return false;
}
<!--- uf --->















return true;

}


</script>



<script>
jQuery(function($){
   $("#formacao-inicio-id-input").mask("99/99/9999");
   $("#formacao-defesa-id-input").mask("99/99/9999");
   $("#cpf").mask("999.999.999-99");
   $("#cep").mask("99999-999");
   $("#telefone").mask("(99) 9999-9999");

});
</script>





<script> 
function checkForOther(obj) { 
 if (!document.layers) { 
 
 var txt = document.getElementById("formacao-inicio"); /// NOME DO ID QUE MOSTRA  ///
 var txt2 = document.getElementById("formacao-defesa"); /// NOME DO ID QUE MOSTRA  ///
 
 var input = document.getElementById("formacao-inicio-id-input"); // ID DO INPUT text ///
 var input2 = document.getElementById("formacao-defesa-id-input"); // ID DO INPUT text ///
 

 if (obj.value == "Graduando" ^ obj.value == "Especialização - Cursando" ^ obj.value == "Mestrando" ^ obj.value == "Doutorando") { 
 
 txt.style.display = "block";
 txt2.style.display = "none"; 
 
 input.name = "data_inicio";

 input.value = "";

 } else if (obj.value == "Superior Completo" ^ obj.value == "Especialização - Completa" ^ obj.value == "Mestrado" ^ obj.value == "Doutorado") {
	  
 txt2.style.display = "block";
 txt.style.display = "none"; 
 
 <!-- input.name = "data_defesa1"; -->

 input2.value = "";

 
 } else if (obj.value == "Pós-Doutorado" ^ obj.value == "Pós-Doutorando") {
	
	txt2.style.display = "none";
	txt.style.display = "none"; 
	
	input.value = "";
	input2.value = ""; 
	 
 	} else if (obj.value == "Selecione sua Formação") {
	
	txt2.style.display = "none";
	txt.style.display = "none"; 
	
	input.value = "";
	input2.value = ""; 
	 
 	}
 } 
} 
/////////////////////////////////////
</script> 






<script> 
function checkForLeciona(obj) { 
 if (!document.layers) { 
 
 var txt = document.getElementById("tempo-leciona"); /// NOME DO ID QUE MOSTRA  ///
 var txt2 = document.getElementById("nao-leciona"); /// NOME DO ID QUE MOSTRA  ///
 
 var input = document.getElementById("lecione-id"); // ID DO INPUT text ///
 var input2 = document.getElementById("lecione-id2"); // ID DO INPUT text ///

 

 if (obj.value == "sim") { 
 
 txt.style.display = "block";
 txt2.style.display = "none";



 input.value = "";
 

 } else {
	  
 txt.style.display = "none";
 txt2.style.display = "block";



 input.value = "";
 input.value2 = "";

 
 } 
 } 
} 
/////////////////////////////////////
</script> 



<script> 
function checkForRelacoes(obj) { 
 if (!document.layers) { 
 
 var txt = document.getElementById("curso-relacoes"); /// NOME DO ID QUE MOSTRA  ///

 var input = document.getElementById("relacoes-id"); // ID DO INPUT text ///


 if (obj.value == "sim") { 
 
 txt.style.display = "block";



 input.value = "";

 } else {
	  
 txt.style.display = "none";




 input.value = "";


 
 } 
 } 
} 
/////////////////////////////////////
</script> 




<script> 
function checkForCursodistancia(obj) { 
 if (!document.layers) { 
 
 var txt = document.getElementById("horas-duracao"); /// NOME DO ID QUE MOSTRA  ///

 var input = document.getElementById("duracao-curso-id"); // ID DO INPUT text ///


 if (obj.value == "sim") { 
 
 txt.style.display = "block";

 input.value = "";
 

 } else {
	  
 txt.style.display = "none";

 input.value = "";


 
 } 
 } 
} 
/////////////////////////////////////
</script> 



<script> 
function checkForImpedimento(obj) { 
 if (!document.layers) { 
 
 var txt = document.getElementById("impedimento"); /// NOME DO ID QUE MOSTRA  ///

 var input = document.getElementById("impedimento-id"); // ID DO INPUT text ///


 if (obj.value == "sim") { 
 
 txt.style.display = "block";

 input.value = "";
 

 } else {
	  
 txt.style.display = "none";

 input.value = "";


 
 } 
 } 
} 
/////////////////////////////////////
</script> 






</head>

<body onload="Xaprb.InputMask.setupElementMasks()" >

<div id="head-align">
	<div id="head">
		
        <div id="logo-nepre">
        <img src="imagens/logonepre.jpg" alt="" width="166" height="171">
        </div>
        
        <div id="logo-ufmt">
        <img src="imagens/logouab.jpg" alt="" width="151" height="120">
        
        <img src="imagens/logoufmt.jpg" alt="" width="113" height="120">
        </div>
        
  </div>
</div>

<div id="corpo-align">
	<div id="corpo">
    	<div class="form-chama">
        	<h2>Formulario...</h2>
            <span>
            Questiodddd</span>
            
       
        </div>
    
    
    
<div align="center" style="height:300px;" id="formul">
<h1 style="font-family:Arial, Helvetica, sans-serif; color:grey;">Obrigado <?php echo $_GET['nome'] ;?>
</h1>     
<p>
Agradecemos sua participação no preenchimento do formulário.
<br/>
<h4>Seu protocolo é:  <?php echo $_GET['c'] ;?></h4>
</p>
</div><!-- formul end -->
    
    
	</div><!-- corpo end -->
</div>

</body>
</html>